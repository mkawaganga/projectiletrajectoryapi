package de.janst.trajectoryapi;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import de.janst.trajectoryapi.config.PluginConfiguration;
import de.janst.trajectoryapi.listener.BowListener;
import de.janst.trajectoryapi.listener.PlayerListener;
import de.janst.trajectoryapi.playerhandling.PlayerHandler;
import de.janst.trajectoryapi.scheduler.TrajectoryScheduler;

public class ProjectileTrajectoryPlugin extends JavaPlugin implements ProjectileTrajectoryAPI{

	private static ProjectileTrajectoryAPI API;
	
	private PlayerHandler playerHandler;
	private PluginConfiguration config;

	public void onEnable() {
		try {
			File dataFolder = getDataFolder();
			if (!dataFolder.exists()) {
				dataFolder.mkdir();
			}
			this.config = new PluginConfiguration(this, new File(dataFolder, "config.yml"));

			this.playerHandler = new PlayerHandler();

			TrajectoryScheduler trajectoryScheduler = new TrajectoryScheduler(this, playerHandler);
			trajectoryScheduler.start(config.getTickSpeed());

			getServer().getPluginManager().registerEvents(new PlayerListener(this, playerHandler), this);
			getServer().getPluginManager().registerEvents(new BowListener(playerHandler), this);

			API = (ProjectileTrajectoryAPI)this;
			getLogger().info("ProjectileTrajectoryApi enabled!");
		} catch (Exception e) {
			getLogger().info("Failed to enable ProjectileTrajectoryApi!");
			e.printStackTrace();
		}
	}

	public void onDisable() {
		getServer().getScheduler().cancelTasks(this);
	}

	public static ProjectileTrajectoryAPI getTrajectoryAPI() {
		return API;
	}
}
