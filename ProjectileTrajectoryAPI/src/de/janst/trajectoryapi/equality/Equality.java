package de.janst.trajectoryapi.equality;

import org.bukkit.inventory.ItemStack;

public abstract class Equality {

	public boolean equalToItem(ItemStack item) {
		if(item != null) {
			return isEqualToItem(item);
		}
		else {
			return false;
		}
	}
	
	public abstract boolean isEqualToItem(ItemStack item);
}
