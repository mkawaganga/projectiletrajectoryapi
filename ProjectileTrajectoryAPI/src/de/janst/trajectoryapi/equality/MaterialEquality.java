package de.janst.trajectoryapi.equality;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class MaterialEquality extends Equality {

	private Material material;

	public MaterialEquality(Material material) {
		this.material = material;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	@Override
	public boolean isEqualToItem(ItemStack item) {
		if(item.getType() == material)
			return true;
		return false;
	}
}
