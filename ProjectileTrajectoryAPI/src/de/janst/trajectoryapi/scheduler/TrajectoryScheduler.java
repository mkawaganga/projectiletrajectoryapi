package de.janst.trajectoryapi.scheduler;

import java.util.Collection;

import de.janst.trajectoryapi.ProjectileTrajectoryPlugin;
import de.janst.trajectoryapi.playerhandling.PlayerHandler;
import de.janst.trajectoryapi.playerhandling.PlayerObject;

public class TrajectoryScheduler extends AbstractScheduler {

	private final PlayerHandler playerHandler;
	
	public TrajectoryScheduler(ProjectileTrajectoryPlugin trajectoryApi, PlayerHandler playerHandler) {
		super(trajectoryApi);
		this.playerHandler = playerHandler;
	}

	@Override
	public void run() {
		Collection<PlayerObject> playerObjects = playerHandler.getPlayerObjects();

		for (PlayerObject playerObject : playerObjects) {
			if (playerObject.isValidForTrajectoryRender()) {
				playerObject.drawTrajectory();
			}
		}
	}
}
