package de.janst.trajectoryapi.listener;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.janst.trajectoryapi.ProjectileTrajectoryPlugin;
import de.janst.trajectoryapi.playerhandling.PlayerHandler;
import de.janst.trajectoryapi.playerhandling.PlayerObject;

public class PlayerListener implements Listener {

	private PlayerHandler playerHandler;
	private ProjectileTrajectoryPlugin trajectoryApi;

	public PlayerListener(ProjectileTrajectoryPlugin trajectoryApi, PlayerHandler playerHandler) {
		this.playerHandler = playerHandler;
		this.trajectoryApi = trajectoryApi;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		playerHandler.addPlayer(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onItemChange(PlayerItemHeldEvent event) {
		scheduleUpdate(event.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		scheduleUpdate(event.getWhoClicked().getUniqueId());

	}

	@EventHandler
	public void onDrag(InventoryDragEvent event) {
		scheduleUpdate(event.getWhoClicked().getUniqueId());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		playerHandler.removePlayer(event.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		scheduleUpdate(event.getPlayer().getUniqueId());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onThrow(ProjectileLaunchEvent event) {
		if (event.getEntity().getShooter() instanceof Player) {
			Player player = (Player) event.getEntity().getShooter();
			scheduleUpdate(player.getUniqueId());
		}
	}

	@EventHandler
	public void onKick(PlayerKickEvent event) {
		playerHandler.removePlayer(event.getPlayer().getUniqueId());
	}

	private void scheduleUpdate(UUID uuid) {
		PlayerObject playerObject = playerHandler.getPlayerObject(uuid);
		if (playerObject != null) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(trajectoryApi, new Runnable() {

				@Override
				public void run() {
					playerObject.update();

				}
			});
		}
	}
}
