package de.janst.trajectoryapi.calculator;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import de.janst.trajectoryapi.particle.Particle;
import de.janst.trajectoryapi.playerhandling.PlayerObject;

public abstract class TrajectoryCalculator {

	protected int maximalLength;
	protected int minimalDistance;
	protected final PlayerObject playerObject;
	protected final Particle particle;

	public TrajectoryCalculator(PlayerObject playerObject, Particle particle, int minimalDistance, int maximalLength) {
		this.playerObject = playerObject;
		this.particle = particle;
		this.minimalDistance = minimalDistance;
		this.maximalLength = maximalLength;
	}

	public Particle getParticle() {
		return this.particle;
	}

	public void draw() {
		Player player = playerObject.getPlayer();
		Location location = player.getEyeLocation();
		for (Vector vector : getTrajectory()) {
			if (location.getBlock().getType().isSolid())
				break;
			particle.send(player, location.clone().add(vector.multiply(0.5 + Math.random())));
			location.add(vector);
		}
	}

	public abstract List<Vector> getTrajectory();
}
