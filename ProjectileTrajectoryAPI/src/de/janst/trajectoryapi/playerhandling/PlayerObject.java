package de.janst.trajectoryapi.playerhandling;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import de.janst.trajectoryapi.calculator.TrajectoryCalculator;
import de.janst.trajectoryapi.equality.Equality;
import de.janst.trajectoryapi.equality.MaterialEquality;

public class PlayerObject {

	private Player player;
	private HashMap<Equality, TrajectoryCalculator> calculators = new HashMap<>();
	private Equality currentEquality;
	private long playerHoldingBowTime;

	public PlayerObject(Player player) {
		this.player = player;
		update();
	}

	public Player getPlayer() {
		return this.player;
	}

	public void update() {
		ItemStack itemStack = getPlayer().getInventory().getItemInMainHand();
		
		for (Equality equlity : calculators.keySet()) {
			if(equlity.isEqualToItem(itemStack)) {
				this.currentEquality = equlity;
			}
		}
	}

	public TrajectoryCalculator getCalculator() {
		return calculators.get(currentEquality);
	}

	public boolean isValidForTrajectoryRender() {
		if (currentEquality == null)
			return false;
		if(currentEquality instanceof MaterialEquality) {
			MaterialEquality matEquality = (MaterialEquality)currentEquality;
			if(matEquality.getMaterial() == Material.BOW && playerHoldingBowTime == 0)
				return false;
		}
		return true;
	}

	public void drawTrajectory() {
		if (isValidForTrajectoryRender() && getCalculator() != null) {
			getCalculator().draw();
		}
	}

	public void setPlayerHoldingBowTime(long playerHoldingBowTime) {
		this.playerHoldingBowTime = playerHoldingBowTime;
	}

	public long getPlayerHoldingBowTime() {
		return playerHoldingBowTime;
	}
}
