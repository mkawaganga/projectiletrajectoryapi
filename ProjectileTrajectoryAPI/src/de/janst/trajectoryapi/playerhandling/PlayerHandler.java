package de.janst.trajectoryapi.playerhandling;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

public class PlayerHandler {

	private Map<UUID, PlayerObject> playerObjects = new HashMap<UUID, PlayerObject>();

	private void addPlayerObject(PlayerObject playerObject) {
		if (playerObject != null) {
			playerObjects.put(playerObject.getPlayer().getUniqueId(), playerObject);
		}
	}

	public void addPlayer(Player player) {
		if (player != null) {
			PlayerObject playerObject = new PlayerObject(player);
			addPlayerObject(playerObject);
		}
	}

	public void removePlayer(UUID uuid) {
		PlayerObject playerObject = getPlayerObject(uuid);
		if(playerObject != null) {
			playerObjects.remove(uuid);
		}
	}

	public PlayerObject getPlayerObject(UUID uuid) {
		return playerObjects.get(uuid);
	}

	public boolean containsPlayerObject(UUID uuid) {
		return playerObjects.containsKey(uuid);
	}

	public Collection<PlayerObject> getPlayerObjects() {
		return playerObjects.values();
	}
}
