package de.janst.trajectoryapi.config;

import java.io.File;
import java.io.IOException;

import org.bukkit.plugin.java.JavaPlugin;

public class ConfigWithInternalResource extends Config {

	private JavaPlugin plugin;

	public ConfigWithInternalResource(JavaPlugin plugin, File file) throws IOException {
		super(file);
		this.plugin = plugin;
	}
	
	@Override
	public void setupFile() {
		if(!configFile.exists()) {
			hasChanges = true;
			NEWFILE = true;
			plugin.saveResource(configFile.getName(), true);
		}
	}
}
