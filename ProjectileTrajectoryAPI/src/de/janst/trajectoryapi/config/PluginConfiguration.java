package de.janst.trajectoryapi.config;

import java.io.File;
import java.io.IOException;

import org.bukkit.plugin.java.JavaPlugin;

public class PluginConfiguration extends ConfigWithInternalResource {

	public PluginConfiguration(JavaPlugin plugin, File file) throws IOException {
		super(plugin, file);
	}

	public int getTickSpeed() {
		return config.getInt("ticks-between-particles");
	}
	
	public boolean saveInstant() {
		return config.getBoolean("save-instant");
	}

	public int getSaveInterval() {
		return config.getInt("save-interval");
	}
	
	public int getMaximalLength() {
		return config.getInt("maximal-length");
	}
	
	public boolean allowParticleChange() {
		return config.getBoolean("allow-particle-change");
	}
}
