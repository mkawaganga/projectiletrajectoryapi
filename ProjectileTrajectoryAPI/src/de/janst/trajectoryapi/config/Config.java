package de.janst.trajectoryapi.config;

import java.io.File;
import java.io.IOException;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {

	protected File configFile = null;
	protected YamlConfiguration config;
	protected boolean hasChanges = false;
	protected boolean NEWFILE = false;
	
	public Config(File file) throws IOException {
		configFile = file;
		setupFile();
		loadConfig();
	}
	
	public void setupFile() throws IOException {
		if(!configFile.exists()) {
			configFile.createNewFile();
		}
	}
	
	private void loadConfig() {
		config = YamlConfiguration.loadConfiguration(configFile);
	}
	
	public boolean hasChanges() {
		return this.hasChanges;
	}

	public File getFile() {
		return this.configFile;
	}
	
	public YamlConfiguration getYamlConfiguration() {
		return this.config;
	}
}
