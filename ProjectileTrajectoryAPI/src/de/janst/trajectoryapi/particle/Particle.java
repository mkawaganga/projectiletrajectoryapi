package de.janst.trajectoryapi.particle;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface Particle {
	public void send(Player player, Location location);
}
