package de.janst.trajectoryapi.particle;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class BukkitParticle implements Particle {

	private org.bukkit.Particle particleType;
	private int amount;
	private double offsetX;
	private double offsetY;
	private double offsetZ;
	private double speed;
	
	public BukkitParticle(org.bukkit.Particle particleType, int amount, double offsetX, double offsetY, double offsetZ, double speed) {
		this.particleType = particleType;
		this.amount = amount;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.offsetZ = offsetZ;
		this.speed = speed;
	}
	
	@Override
	public void send(Player player, Location location) {
		player.spawnParticle(particleType, location, amount, offsetX, offsetY, offsetZ, speed);
	}

}
